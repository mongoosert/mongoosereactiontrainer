//
//  FixedViewController.h
//  MongooseReactionTrainer
//
//  Created by Robert Handsfield on 9/29/13.
//  Copyright (c) 2013 Robert Handsfield. All rights reserved.
//


#import <UIKit/UIKit.h>
#import <QuartzCore/CAAnimation.h>
#import <AudioToolbox/AudioToolbox.h>

#include <assert.h>
#include <mach/mach.h>
#include <mach/mach_time.h>


@interface FixedViewController : UIViewController
{
    UILabel *timeLabel_;
    UILabel *roundLabel_;
    
    NSTimer *sleepTimer_;
    NSTimer *prepTimer_;
    
    UITextField *roundField_;
    UITextField *prepField_;
    UITextField *intervalField_;
   
    UIButton *runButton_;
    UIButton *stopButton_;
}

//View Object Properties
@property (retain, nonatomic) IBOutlet UILabel *roundLabel;
@property (retain, nonatomic) IBOutlet UILabel *timeLabel;

@property (retain, nonatomic) IBOutlet UITextField *roundField;
@property (retain, nonatomic) IBOutlet UITextField *prepField;
@property (retain, nonatomic) IBOutlet UITextField *intervalField;

@property (retain,nonatomic) NSTimer *sleepTimer;
@property (retain,nonatomic) NSTimer *prepTimer;

-(IBAction)textFieldReturn:(id)sender;

//runButton Method - updates labels, calls prepTimer
-(IBAction)runButtonPressed:(id)sender;

//executes @ 1s- counts down prep time, then calls sleepTimer
-(void)prepTimeMethod:(NSTimer *)prepTimer;

//counts via system time, updates labels
-(void)sleepTimeMethod:(NSTimer *)sleepTimer;

//stopButton method- kills timers and resets labels
-(IBAction)stopButtonPressed:(id)sender;

//file must be an mp3, arg does not include extension
-(void)playSoundMethod:(NSString *)soundFileName;


@end

//
//  Accelerometer.h
//  MongooseReactionTrainer
//
//  Created by Robert Handsfield on 10/7/13.
//  Copyright (c) 2013 Robert Handsfield. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <CoreMotion/CoreMotion.h>


@interface Accelerometer : NSObject

-(void)startMeasuring:(CMAccelerometerData *)something;

@property(readonly,nonatomic) CMAcceleration acceleration;


@end

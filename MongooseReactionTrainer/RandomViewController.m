//
//  RandomViewController.m
//  MongooseReactionTrainer
//
//  Created by Robert Handsfield on 9/29/13.
//  Copyright (c) 2013 Robert Handsfield. All rights reserved.
//

#import "RandomViewController.h"

@interface RandomViewController ()

@property (nonatomic,assign) int currentRound;
@property (nonatomic,assign) int maxRounds;
@property (nonatomic,assign) int prepTime;

@property (nonatomic,assign) double startTime;
@property (nonatomic,assign) double currentTime;
@property (nonatomic,assign) double elapsedTime;
@property (nonatomic,assign) double beepTime;
@property (nonatomic,assign) double reactionTime;
@property (nonatomic,assign) double roundTime;
@property (nonatomic,assign) double minTime;
@property (nonatomic,assign) double maxTime;

@end

@implementation RandomViewController

- (void)viewDidLoad
{
    [super viewDidLoad];
	// Do any additional setup after loading the view, typically from a nib.
    self.roundField.text = @"2";
    self.prepField.text = @"3";
    self.minIntervalField.text = @"3";
    self.maxIntervalField.text = @"5";
    
    //initialize accelerometer stuff
    accX = 10;
    accY = 10;
    accZ = 10;
    
    //create a CMMotionManager object;
    //this object calls methods to receive motion data
    self.motionManager = [[CMMotionManager alloc] init];
    self.motionManager.deviceMotionUpdateInterval = .005;
    //[self.motionManager startDeviceMotionUpdates];

    NSLog(@"%hhd",self.motionManager.deviceMotionAvailable);
    
    accX = self.motionManager.deviceMotion.userAcceleration.x;
    accY = self.motionManager.deviceMotion.userAcceleration.y;
    accZ = self.motionManager.deviceMotion.userAcceleration.z;
    
    NSLog(@"X= %f, Y= %f, Z= %f", accX, accY, accZ);
    
    
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

-(double)getAcc:(double)x :(double)y :(double)z
{
    /*
     double x=0.;
     double y=0.;
     double z=0.;
     
     
     x = self.motionManager.deviceMotion.userAcceleration.x;
     y = self.motionManager.deviceMotion.userAcceleration.y;
     z = self.motionManager.deviceMotion.userAcceleration.z;
     */
    
    double acc;
    acc = sqrt( pow(x, 2) + pow(y, 2) + pow(z, 2) );
    
    NSLog(@"X= %f, Y= %f, Z= %f, Acc= %f", x, y, z, acc);
    
    return acc;
}


//runButton method- updates labels, calls prepTimer
- (IBAction)runButtonPressed:(id)sender
{
    //kill any running timers
    [self.prepTimer invalidate];
    [self.sleepTimer invalidate];
    
    //reset the motion manager
    [self.motionManager stopDeviceMotionUpdates];
    
    //set the current round to 1
    self.currentRound = 1;
    
    //initialize elapsed time
    self.elapsedTime = 0;
    
    //get rounds from roundField
    self.maxRounds = [self.roundField.text intValue];
    //NSLog(@"maxRounds = %d", self.maxRounds);
    
    //get prep time from prepField
    self.prepTime = [self.prepField.text intValue];
    //NSLog(@"prepTime = %d", self.prepTime);
    
    //get min time from minIntervalField
    self.minTime = [self.minIntervalField.text floatValue];
    //NSLog(@"minTime = %f", self.minTime);
    
    //get max time from maxIntervalField
    self.maxTime = [self.maxIntervalField.text floatValue];
    //NSLog(@"maxTime = %f", self.maxTime);
    
    //get round time from random number generator
    self.roundTime = [self randomNumberGenerator:self.minTime max:self.maxTime];
    //NSLog(@"roundTime = %f", self.roundTime);
    
    //update the roundLable
    self.roundLabel.text = [NSString stringWithFormat:@"Round: %d / %d",self.currentRound, self.maxRounds];
    
    //display the prepTime in the timeLabel
    self.timeLabel.text = [NSString stringWithFormat:@"Time: %@", self.prepField.text];
    
    
    //invoke the prepTimer
    self.prepTimer = [NSTimer scheduledTimerWithTimeInterval:1. target:self selector:@selector(prepTimeMethod:) userInfo:nil repeats:YES];
    
    //activate the motion listener
    [self.motionManager startDeviceMotionUpdates];

}

//fires @ 1s- counts down prep time, calls sleepTimer
- (void)prepTimeMethod:(NSTimer *)prepTimer
{
    //decrement the prepTime by 1s
    self.prepTime--;
    
    //display the new prepTime in the time label
    self.timeLabel.text = [NSString stringWithFormat:@"Time: %d", self.prepTime];
    
    //count down last 3 prep seconds w/ tick sound
    if (self.prepTime < 4 && self.prepTime > 0) {
        
        //play the tick sound
        [self playSoundMethod:@"tick - 1s"];
        
    }
    //when prep ends
    else if (self.prepTime == 0) {
        
        //get the stop watch starting time
        self.startTime = CACurrentMediaTime();
        
        //call the sleepTimer
        self.sleepTimer = [NSTimer scheduledTimerWithTimeInterval:.005 target:self selector:@selector(sleepTimeMethod:) userInfo:nil repeats:YES];
        
        //display round 1 in the roundLabel
        self.roundLabel.text = [NSString stringWithFormat:@"Round: %d / %d",self.currentRound, self.maxRounds];
        
        //call the motion listener
        [self motionListener:_motionManager];
        
        //stop the prepTimer
        [self.prepTimer invalidate];
    }
    
}

//fires every 5ms, counts via system time, updates labels
- (void)sleepTimeMethod:(NSTimer *)sleepTimer
{

    do //for the current round
    {
        //calculate the current elapsed time
        self.elapsedTime = CACurrentMediaTime() - self.startTime;
     
        //get last acc valued
        accX = self.motionManager.deviceMotion.userAcceleration.x;
        accY = self.motionManager.deviceMotion.userAcceleration.y;
        accZ = self.motionManager.deviceMotion.userAcceleration.z;
        
        userAcc = sqrt( pow(accX, 2) + pow(accY, 2) + pow(accZ, 2) );
        
        //if acc has happened, show the rxn time, kill timers
        if (userAcc >= 3.) {
            self.reactionTime = CACurrentMediaTime() - self.beepTime;
            NSLog(@"Critical Acc!  Acc = %f,  RTime = %f", userAcc, self.reactionTime);
            
            self.timeLabel.text = [NSString stringWithFormat:@"Time: %.2f",self.reactionTime];

            //kill the motion listener
            [self.motionManager stopDeviceMotionUpdates];
            //kill the timer
            [self.sleepTimer invalidate];
        }
        
        //if round hasn't ended
        if (self.elapsedTime <= self.roundTime) {
            
            //update the clock label w/ the elapsed time
            self.timeLabel.text = [NSString stringWithFormat:@"Time: %.2f",self.elapsedTime];
            
        }
        //if round has ended
        else {
            //increment the round
            self.currentRound++;
            NSLog(@"currentRound = %d",self.currentRound);
            
            //get an interval for the next round
            self.roundTime = [self randomNumberGenerator:self.minTime max:self.maxTime];
            NSLog(@"roundTime = %f",self.roundTime);
            
            //reset the elapsed time
            self.elapsedTime = 0;
            
            //get a new interval starting time
            self.startTime = CACurrentMediaTime();
            
            //update the current round label
            if (self.currentRound <= self.maxRounds) {
                self.roundLabel.text = [NSString stringWithFormat:@"Round: %d / %d",self.currentRound, self.maxRounds];
            }
            
            
            //if this is the last round
            if (self.currentRound >= self.maxRounds+1) {
                [self playSoundMethod:@"end beep"];
                [self.sleepTimer invalidate];
                
                //stop the accelerometer
                [self.motionManager stopDeviceMotionUpdates];
                break;
            }
            //if this is not the last round
            else {
                [self playSoundMethod:@"beep-7"];
                self.beepTime = CACurrentMediaTime();
            }
        }
        
    }while(self.currentRound >= self.maxRounds+1);
    //not sure why this loop condition must be so complicated to work

}

//stopButton method- kills timers and resets labels
-(IBAction)stopButtonPressed:(id)sender
{
    //NSLog(@"timers stopped");
    
    //kill any running timers
    [self.prepTimer invalidate];
    [self.sleepTimer invalidate];
    
    //set the current round to 1
    self.currentRound = 1;
    
    //initialize elapsed time
    self.elapsedTime = 0;
    
    //update the roundLable
    //self.roundLabel.text = [NSString stringWithFormat:@"Round: %d / %d",self.currentRound, self.maxRounds];
    self.roundLabel.text = [NSString stringWithFormat:@"Round: "];

    
    //update the timeLabel
    //self.timeLabel.text = [NSString stringWithFormat:@"Time: %@", self.prepField.text];
    self.timeLabel.text = [NSString stringWithFormat:@"Time: "];
}

- (void)motionListener:(CMMotionManager *)aMotionManager
{
    
    
    
}


- (IBAction)textFieldReturn:(id)sender
{
    [sender resignFirstResponder];
}

- (void)playSoundMethod:(NSString *)soundFileName
{
    
    //arg must be an mp3
    
    SystemSoundID soundID;
    
    NSString *soundPath = [[NSBundle mainBundle] pathForResource:soundFileName ofType:@"mp3"];
    
    NSURL *soundPathURL = [NSURL fileURLWithPath:soundPath];
    
    AudioServicesCreateSystemSoundID((__bridge CFURLRef) soundPathURL, &soundID);
    
    AudioServicesPlaySystemSound(soundID);
}

//returns a random number within the domain min-max
- (double)randomNumberGenerator:(double)min max:(double)max
{
    double randNum;
    
    //http://iphonedevelopment.blogspot.com/2008/10/random-thoughts-rand-vs-arc4random.html
    randNum = min + ((double)arc4random() / 0x100000000) * (max - min);
    
    return randNum;
}



@end

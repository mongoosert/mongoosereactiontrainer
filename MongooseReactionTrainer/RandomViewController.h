//
//  RandomViewController.h
//  MongooseReactionTrainer
//
//  Created by Robert Handsfield on 9/29/13.
//  Copyright (c) 2013 Robert Handsfield. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <QuartzCore/CAAnimation.h>
#import <AudioToolbox/AudioToolbox.h>
#import <CoreMotion/CoreMotion.h>

#include <assert.h>
#include <mach/mach.h>
#include <mach/mach_time.h>

double accX;
double accY;
double accZ;
double userAcc;

@interface RandomViewController : UIViewController
{
    UILabel *timeLabel_;
    UILabel *roundLabel_;
   
    NSTimer *sleepTimer_;
    NSTimer *prepTimer_;
    
    UITextField *roundField_;
    UITextField *prepField_;
    UITextField *minIntervalField_;
    UITextField *maxIntervalField_;
    
    UIButton *runButton_;
    UIButton *stopButton_;
}

//View Object Properties
@property (retain,nonatomic) IBOutlet UILabel *roundLabel;
@property (retain,nonatomic) IBOutlet UILabel *timeLabel;

@property (retain,nonatomic) IBOutlet UITextField *roundField;
@property (retain,nonatomic) IBOutlet UITextField *prepField;
@property (retain,nonatomic) IBOutlet UITextField *minIntervalField;
@property (retain,nonatomic) IBOutlet UITextField *maxIntervalField;

@property (retain,nonatomic) NSTimer *sleepTimer;
@property (retain,nonatomic) NSTimer *prepTimer;

@property (strong,nonatomic) CMMotionManager *motionManager;
@property (strong,nonatomic) CMDeviceMotion *deviceMotionListener;
@property(readonly, nonatomic) CMAcceleration userAcceleration;

//Hides keyboard when exiting text field
-(IBAction)textFieldReturn:(id)sender;

//runButton method- updates labels, calls prepTimer
-(IBAction)runButtonPressed:(id)sender;

//executes @ 1s- counts down prep time, calls sleepTimer
-(void)prepTimeMethod:(NSTimer *)prepTimer;

//counts via system time, updates labels
-(void)sleepTimeMethod:(NSTimer *)sleepTimer;

//stopButton method- kills timers and resets labels
-(IBAction)stopButtonPressed:(id)sender;

-(void)playSoundMethod:(NSString *)soundFileName;

-(double)randomNumberGenerator:(double)min max:(double)max;

//listens for accelerations and does stuff
-(void)motionListener:(CMMotionManager *) aMotionManager;

-(double)getAcc:(double)x :(double)y :(double)z;    

@end
